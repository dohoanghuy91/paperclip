class UsersController < ApplicationController
  def create
    @user = User.create(user_params)

    if @user
      redirect_to @user
    else
      flash[:notice] = "Can't create new user !"
      redirect_to :new
    end

  end

  def edit
  end

  def new
  end

  def show
    @user = User.find_by id: params[:id]
  end

  def index
    @users = User.all
  end

  def down_load_avatar
    user = User.find_by id: params[:id]
    # slice the string to get download link
    # before: /system/users/avatars/000/000/002/original/anh02.png?1479974141
    # after: /system/users/avatars/000/000/002/original/anh02.png
    origin_url = user.avatar.url.to_s
    image_url = origin_url.slice(0, origin_url.rindex('?'))

    image_url = File.join(Rails.root,"public", image_url)
    test_url = File.join(Rails.root,"public",user.avatar.url(:original, true))
    send_file(test_url,
              filename: "#{user.name}.png",
              type: "application/png")
  end

  private
    def user_params
      params.require(:user).permit!
    end
end
