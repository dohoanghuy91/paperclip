Rails.application.routes.draw do
  root "users#new"
  post '/download' => 'users#down_load_avatar'
  resources :users
end
